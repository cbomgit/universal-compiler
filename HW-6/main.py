import Grammar
import TableGenerator
import pdb


grammar_file = input("file to read production rules from (file ends in .txt): ")
g = Grammar.Grammar(grammar_file)
print("{:<50s}{:<50s}{:>50s}".format("non-term", "first sets","follow sets"))
for p in g.productions:
    print("{:<50s}{:<50s}{:>50s}".format(str(p['lhs']), str(g.first_sets[p['lhs']]), str(g.follow_sets[p['lhs']])))

print("\n\n\n")
print("{:<50s}{:>20s}".format("production", "predict set"))
for p in g.productions:
    print("{:<50s}{:>20s}".format(str(p['rhs']).strip("\n"), str(p['predict'])))