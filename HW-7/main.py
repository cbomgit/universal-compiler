import Grammar
import TableGenerator
import pdb
import pprint


grammar_file = input("file to read production rules from (file ends in .txt): ")
g = Grammar.Grammar(grammar_file)
ll1_table = TableGenerator.TableGenerator(g)
pprint.pprint(ll1_table.table)