import Grammar


class TableGenerator:

    def __init__(self, grammar):
        self.table = self.generate_table(grammar)

    
    def generate_table(self, grammar):
        table = {}
        ndx = 0
        for prd in grammar.productions:
            table[prd['lhs']] = {}
        
        for prd in grammar.productions:
            for t in grammar.terminals:
                table[prd['lhs']][t] = None
        
        for prd in grammar.productions:
            for terminal in prd['predict']:
                table[prd['lhs']][terminal] = ndx
            ndx = ndx + 1
     
        return table
        

