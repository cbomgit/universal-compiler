class MicroScanner:

    """Microscanner class parses input and generates tokens for grammar"""
    def __init__(self, file_name):
        file = open(file_name, 'r')
        with open('program.txt','r') as file:
            self.input_string = file.read()
            self.input_cursor = 0
            self.token_text = ""

    MOVE_APPEND = "MoveAppend"
    MOVE_NO_APPEND = "MoveNoAppend"
    HALT_APPEND = "HaltAppend"
    HALT_NO_APPEND = "HaltNoAppend"
    HALT_REUSE = "HaltReuse"
    ERROR = "Error"

    state_action_map = {
        "$abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ": {
            0:{"state":1, "action":MOVE_APPEND},
            1:{"state":1, "action":MOVE_APPEND},
            2:{"state":12, "action":HALT_REUSE},
            3:{"state":13, "action":HALT_REUSE},
            4:{"state":21, "action":HALT_REUSE},
            5:{"state":5, "action":MOVE_NO_APPEND},
            6:{"state":-1, "action":ERROR}
        },
        "0123456789": {
            0:{"state":2, "action":MOVE_APPEND},
            1:{"state":1, "action":MOVE_APPEND},
            2:{"state":2, "action":MOVE_APPEND},
            3:{"state":13, "action":HALT_REUSE},
            4:{"state":21, "action":HALT_REUSE},
            5:{"state":5, "action":MOVE_NO_APPEND},
            6:{"state":-1, "action":ERROR}
        },
        " ": {
            0:{"state":3, "action":MOVE_NO_APPEND},
            1:{"state":11, "action":HALT_NO_APPEND},
            2:{"state":12, "action":HALT_NO_APPEND},
            3:{"state":3, "action":MOVE_NO_APPEND},
            4:{"state":21, "action":HALT_NO_APPEND},
            5:{"state":5, "action":MOVE_NO_APPEND},
            6:{"state":-1, "action":ERROR}
        },
        "+": {
            0:{"state":14, "action":HALT_NO_APPEND},
            1:{"state":11, "action":HALT_REUSE},
            2:{"state":12, "action":HALT_REUSE},
            3:{"state":13, "action":HALT_REUSE},
            4:{"state":21, "action":HALT_REUSE},
            5:{"state":5, "action":MOVE_NO_APPEND},
            6:{"state":-1, "action":ERROR}
        },
        "-": {
            0:{"state":4, "action":MOVE_APPEND},
            1:{"state":11, "action":HALT_REUSE},
            2:{"state":12, "action":HALT_REUSE},
            3:{"state":13, "action":HALT_REUSE},
            4:{"state":5, "action":MOVE_NO_APPEND},
            5:{"state":5, "action":MOVE_NO_APPEND},
            6:{"state":-1, "action":ERROR}
        },
        "=": {
            0:{"state":-1, "action":ERROR},
            1:{"state":11, "action":HALT_REUSE},
            2:{"state":12, "action":HALT_REUSE},
            3:{"state":13, "action":HALT_REUSE},
            4:{"state":21, "action":HALT_REUSE},
            5:{"state":5, "action":MOVE_NO_APPEND},
            6:{"state":16, "action":HALT_APPEND}
        },
        ":": {
            0:{"state":6, "action":MOVE_APPEND},
            1:{"state":11, "action":HALT_REUSE},
            2:{"state":12, "action":HALT_REUSE},
            3:{"state":13, "action":HALT_REUSE},
            4:{"state":21, "action":HALT_REUSE},
            5:{"state":5, "action":MOVE_NO_APPEND},
            6:{"state":-1, "action":ERROR}
        },
        ",": {
            0:{"state":17, "action":HALT_APPEND},
            1:{"state":11, "action":HALT_REUSE},
            2:{"state":12, "action":HALT_REUSE},
            3:{"state":13, "action":HALT_REUSE},
            4:{"state":21, "action":HALT_REUSE},
            5:{"state":5, "action":MOVE_NO_APPEND},
            6:{"state":-1, "action":ERROR}
        },
        ";": {
            0:{"state":18, "action":HALT_APPEND},
            1:{"state":11, "action":HALT_REUSE},
            2:{"state":12, "action":HALT_REUSE},
            3:{"state":13, "action":HALT_REUSE},
            4:{"state":21, "action":HALT_REUSE},
            5:{"state":5, "action":MOVE_NO_APPEND},
            6:{"state":-1, "action":ERROR}
        },
        "(": {
            0:{"state":19, "action":HALT_APPEND},
            1:{"state":11, "action":HALT_REUSE},
            2:{"state":12, "action":HALT_REUSE},
            3:{"state":13, "action":HALT_REUSE},
            4:{"state":21, "action":HALT_REUSE},
            5:{"state":5, "action":MOVE_NO_APPEND},
            6:{"state":-1, "action":ERROR}
        },
        ")": {
            0:{"state":20, "action":HALT_APPEND},
            1:{"state":11, "action":HALT_REUSE},
            2:{"state":12, "action":HALT_REUSE},
            3:{"state":13, "action":HALT_REUSE},
            4:{"state":21, "action":HALT_REUSE},
            5:{"state":5, "action":MOVE_NO_APPEND},
            6:{"state":-1, "action":ERROR}
        },
        "_": {
            0:{"state":-1, "action":ERROR},
            1:{"state":1, "action":MOVE_APPEND},
            2:{"state":12, "action":HALT_REUSE},
            3:{"state":13, "action":HALT_REUSE},
            4:{"state":-1, "action":ERROR},
            5:{"state":5, "action":MOVE_NO_APPEND},
            6:{"state":-1, "action":ERROR}
        },
        "\t": {
            0:{"state":3, "action":MOVE_NO_APPEND},
            1:{"state":11, "action":HALT_NO_APPEND},
            2:{"state":12, "action":HALT_NO_APPEND},
            3:{"state":3, "action":MOVE_NO_APPEND},
            4:{"state":21, "action":HALT_NO_APPEND},
            5:{"state":5, "action":MOVE_NO_APPEND},
            6:{"state":-1, "action":ERROR}
        },
        "\n": {
            0:{"state":3, "action":MOVE_NO_APPEND},
            1:{"state":11, "action":HALT_NO_APPEND},
            2:{"state":12, "action":HALT_NO_APPEND},
            3:{"state":3, "action":MOVE_NO_APPEND},
            4:{"state":21, "action":HALT_NO_APPEND},
            5:{"state":15, "action":HALT_NO_APPEND},
            6:{"state":-1, "action":ERROR}
        },
        "?": {
            0:{"state":-1, "action":ERROR},
            1:{"state":-1, "action":ERROR},
            2:{"state":-1, "action":ERROR},
            3:{"state":-1, "action":ERROR},
            4:{"state":-1, "action":ERROR},
            5:{"state":5, "action":MOVE_NO_APPEND},
            6:{"state":-1, "action":ERROR}
        },
    }

    #Constant values for tokens
    #token code constants
    ID = 0
    COLON = 1
    ASSIGNOP = 2
    DOLLARSIGN = 6
    LAMBDA = 7


    #used in transition table to tranlsate final states to tokens

    token_codes = {
        11: ID,

    }

   
    TOKEN_FRIENDLY_TABLE = {
        ID:"Id",
        COLON:":",
        ASSIGNOP:":=",
        DOLLARSIGN:"$",
        LAMBDA:"Lambda"
    }

    exception_table = {
        "Lambda":LAMBDA,
        "$":DOLLARSIGN
    }

    def current_char(self):
        """ returns current character in input stream without consuming it """
        return self.input_string[self.input_cursor]


    def consume_char(self):
        """Consumes the current character in the input stream and returns it """
        ret = self.input_string[self.input_cursor]
        self.input_cursor += 1
        return ret

    def get_remaining_input(self):
        """ returns the remaining input left to scan """
        return self.input_string[self.input_cursor:]

    def get_state_action_pair(self, curr_state, curr_char):

        """ Returns the next state and action for a given state and character """
        pair = [value for key, value in MicroScanner.state_action_map.items() if curr_char in key]
        if len(pair) == 0:
            return MicroScanner.state_action_map['?'][curr_state]
        else:
            return pair[0][curr_state]

    def lookup_code(self, curr_state, curr_char):
        """ Returns the token code for the given state and character """
        state_action_pair = self.get_state_action_pair(curr_state, curr_char)
        next_state = state_action_pair['state']
        return MicroScanner.token_codes[next_state]

    def check_exceptions(self, code):
        """ Matches the token_text with reserved words and symbols """
        if self.token_text.upper() in MicroScanner.exception_table:
            return MicroScanner.exception_table[self.token_text.upper()]
        else:
            return code


    def move_append(self, state, code=0):
        """ Add the current character to the token and consume it. Do not return a code """
        state = self.get_state_action_pair(state, self.current_char())['state']
        self.token_text += self.current_char()
        self.consume_char()
        return {"state":state, "code":None, "text":self.token_text}

    def move_no_append(self, state, code=0):
        """ Consume the current character and do not add it to the token. Do not return a code """
        state = self.get_state_action_pair(state, self.current_char())['state']
        self.consume_char()
        return {"state":state, "code":None, "text":self.token_text}


    def halt_append(self, state, code=0):
        """ Append the current char to the token and return a token code """
        code = self.lookup_code(state, self.current_char())
        self.token_text += self.current_char()
        code = self.check_exceptions(code)
        self.consume_char()
        if code == 0:
            code = self.next_token(code)
        return {"state":state, "code":code, "text":self.token_text}

    def halt_no_append(self, state, code=0):
        """ Return the token without appending the current char to the token string """
        code = self.lookup_code(state, self.current_char())
        code = self.check_exceptions(code)
        self.consume_char()
        if code == 0:
            code = self.next_token(code)
        return {"state":state, "code":code, "text":self.token_text}

    def halt_reuse(self, state, code=0):
        """ Return the token without consuming or appending current char """
        code = self.lookup_code(state, self.current_char())
        code = self.check_exceptions(code)
        if code == 0:
            code = self.next_token(code)
        return {"state":state, "code":code, "text":self.token_text}

    def lexical_error(self, state, code=0):
        print("Lexical error")
        exit()

    action_table = {
        ERROR:lexical_error,
        MOVE_APPEND:move_append,
        MOVE_NO_APPEND:move_no_append,
        HALT_APPEND:halt_append,
        HALT_NO_APPEND:halt_no_append,
        HALT_REUSE:halt_reuse
    }

    def scan(self): 
        self.token_text = ''
        return self.next_token()

    def next_token(self, code=0):
        """ returns the next Token in the input stream """
        state = 0
        if self.current_char() == '\n':
            return "EOP"
        while self.current_char() != '$':
            next_action = self.get_state_action_pair(state, self.current_char())['action']
            return_val = MicroScanner.action_table[next_action](self, state, code)
            self.token_text = return_val['text']
            state = return_val["state"]
            if return_val["code"]:
                return return_val["code"]
        return MicroScanner.DOLLARSIGN



