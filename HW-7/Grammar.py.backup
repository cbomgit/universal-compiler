import re
import pdb

class Grammar:
    """ A class representing a grammar """

    #filename is the path to a file containg the list of productions describing this grammar

    def __init__(self, filename):

        """ Productions is an array of dicts of the form :
            { lhs: 'left hand side of production',
              rhs: 'right hand side of production',
              predict: set()
            }
        """

        self.productions = []

        #terminal symbols in this grammar
        self.terminals = set()

        #non terminal symbols in this grammar
        self.non_terminals = set()

        #all symbols in this grammar (union of terminal and non-terminal sets)
        self.symbols = set()

        #first sets for all symbols
        self.first_sets = {}

        #follow sets for all symbols
        self.follow_sets = {}

        #predict sets for all symbols - {'symbol': set()}
        self.predict_sets = {}

        #symbols found to derive lambda are marked as true in this set,
        #otherwise they are marked as false
        # {'symbol':True or False}
        self.derives_lambda = {}

        self.read_productions(filename)
        self.scan_for_terminals()
        self.symbols = self.terminals | self.non_terminals
        self.mark_lambda()
        self.fill_first()
        self.fill_follow()
        self.generate_predict()
    
    def production_nonterm_goes_to_term(self, non_terminal, terminal):
        for production in self.productions:
            if production['lhs'] == non_terminal and production['rhs'].find(terminal) == 0:
                return True
        return False
                
        
    def mark_lambda(self):
        changes = True
        rhs_derives_lambda = True

        for symbol in self.symbols:
            self.derives_lambda[symbol] = False

        while changes:
            changes = False
            for production in self.productions:
                rhs_derives_lambda = True
                symbols_in_rhs = [x for x in self.symbols if x in production['rhs']]
                for symbol in symbols_in_rhs:
                    rhs_derives_lambda = rhs_derives_lambda and symbol == 'Lambda'
                if rhs_derives_lambda and not self.derives_lambda[production['lhs']]:
                    changes = True
                    self.derives_lambda[production['lhs']] = True

    def non_term_in_rhs(self, non_term):
        result = []
        for p in self.productions:
            if p['rhs'].find(non_term) != -1:
                result.append(p)

    def symbols_in_string(self, string_of_symbols, valid_symbols):
        index_tuples = []
        for sym in valid_symbols:
            start_index = string_of_symbols.find(sym)
            if start_index > -1:
                tup = (start_index, start_index + len(sym))
                index_tuples.append(tup)
        index_tuples.sort()
        
        found_symbols = []
        for tup in index_tuples:
            found_symbol = string_of_symbols[tup[0]:tup[1]]
            found_symbols.append(found_symbol)
        return found_symbols

    def compute_first(self, string):
        symbols = self.symbols_in_string(string, self.symbols)
        k = len(symbols)
        result = set()
        if k == 0:
            result.add('Lambda')
        else:
            result = self.first_sets[symbols[0]].copy() - {'Lambda'}
            i = 0
            while i < k and 'Lambda' in self.first_sets[symbols[i]]:
                result = result | (self.first_sets[symbols[i]] - {'Lambda'})
                i = i + 1
            if i == k and 'Lambda' in self.first_sets[symbols[k - 1]]:
                result = result | {'Lambda'}
        return result

        
    def fill_first(self):
        for non_term in self.non_terminals:
            if self.derives_lambda[non_term]:
                self.first_sets[non_term] = set({'Lambda'})
            else:
                self.first_sets[non_term] = set()
        for term in self.terminals:
            self.first_sets[term] = set({term})
            for non_term in self.non_terminals:
               if self.production_nonterm_goes_to_term(non_term, term):
                   self.first_sets[non_term] = self.first_sets[non_term] | set({term})
        changes = True
        while changes:
            changed_sets = 0
            for production in self.productions:
                temp_first = self.first_sets[production['lhs']].copy()
                self.first_sets[production['lhs']] = self.first_sets[production['lhs']] | self.compute_first(production['rhs'])
                if temp_first != self.first_sets[production['lhs']]:
                    changed_sets = changed_sets + 1
            if changed_sets == 0:
                changes = False


    def fill_follow(self):
        for lhs_nonterms in self.non_terminals:
            self.follow_sets[lhs_nonterms] = set()
        self.follow_sets[self.productions[self.start_symbol]['lhs']] = set({'Lambda'})
        changes = True
        while changes:
            changed_sets = 0
            for n in self.non_terminals:
                productions = self.non_term_in_rhs(n)
                for p in self.productions:
                    rhs = p['rhs']
                    n_locations = [(m.start(), m.end()) for m in re.finditer(n, rhs)]
                    for tup in n_locations:
                        y = rhs[tup[1]:].strip(" ")
                        temp_follow = self.follow_sets[n].copy()
                        self.follow_sets[n] = self.follow_sets[n] | (self.compute_first(y) - {'Lambda'})
                        if 'Lambda' in self.compute_first(y):
                            self.follow_sets[n] = self.follow_sets[n] | self.follow_sets[p['lhs']]
                        if temp_follow != self.follow_sets[n]:
                            changed_sets = changed_sets + 1
            if changed_sets == 0:
                changes = False

    def generate_predict(self):
        for production in self.productions:
            symbols_in_rhs = self.symbols_in_string(production['rhs'], self.symbols)
            if len(symbols_in_rhs) > 0:
                first_symbol = symbols_in_rhs[0]
            if 'Lambda' in self.first_sets[first_symbol]:
                production['predict'] = (self.first_sets[first_symbol] | self.follow_sets[production['lhs']]) - {'Lambda'}
            else:
                production['predict'] = self.first_sets[first_symbol]


    def read_productions(self, file):
        """ Breaks down the grammar and populates the data objects created
            in the constructor
        """
        #get all productions and non-terminals
        f = open(file, "r")
        i = 0
        for line in f:
            r = line.split(" -> ")[1]
            l= line.split(" -> ")[0]
            matches = re.findall(r"<[\w ]+>", r)
            self.non_terminals.add(l)
            self.productions.append({"lhs":l,"rhs":r})
        self.start_symbol = 0
        f.close()

    def scan_for_terminals(self):
        
        #action constants
        MOVE_APPEND = "MoveAppend"
        MOVE_NO_APPEND = "MoveNoAppend"
        HALT_APPEND = "HaltAppend"
        HALT_NO_APPEND = "HaltNoAppend"
        HALT_REUSE = "HaltReuse"
        ERROR = "Error"

        state_action_map = {
            "$abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ": {
                0:{"state":1, "action":MOVE_APPEND},
                1:{"state":1, "action":MOVE_APPEND},
                2:{"state":12, "action":HALT_REUSE},
                3:{"state":13, "action":HALT_REUSE},
                4:{"state":21, "action":HALT_REUSE},
                5:{"state":5, "action":MOVE_NO_APPEND},
                6:{"state":-1, "action":ERROR}
            },
            "0123456789": {
                0:{"state":2, "action":MOVE_APPEND},
                1:{"state":1, "action":MOVE_APPEND},
                2:{"state":2, "action":MOVE_APPEND},
                3:{"state":13, "action":HALT_REUSE},
                4:{"state":21, "action":HALT_REUSE},
                5:{"state":5, "action":MOVE_NO_APPEND},
                6:{"state":-1, "action":ERROR}
            },
            " ": {
                0:{"state":3, "action":MOVE_NO_APPEND},
                1:{"state":11, "action":HALT_NO_APPEND},
                2:{"state":12, "action":HALT_NO_APPEND},
                3:{"state":3, "action":MOVE_NO_APPEND},
                4:{"state":21, "action":HALT_NO_APPEND},
                5:{"state":5, "action":MOVE_NO_APPEND},
                6:{"state":-1, "action":ERROR}
            },
            "+": {
                0:{"state":14, "action":HALT_NO_APPEND},
                1:{"state":11, "action":HALT_REUSE},
                2:{"state":12, "action":HALT_REUSE},
                3:{"state":13, "action":HALT_REUSE},
                4:{"state":21, "action":HALT_REUSE},
                5:{"state":5, "action":MOVE_NO_APPEND},
                6:{"state":-1, "action":ERROR}
            },
            "-": {
                0:{"state":4, "action":MOVE_APPEND},
                1:{"state":11, "action":HALT_REUSE},
                2:{"state":12, "action":HALT_REUSE},
                3:{"state":13, "action":HALT_REUSE},
                4:{"state":5, "action":MOVE_NO_APPEND},
                5:{"state":5, "action":MOVE_NO_APPEND},
                6:{"state":-1, "action":ERROR}
            },
            "=": {
                0:{"state":-1, "action":ERROR},
                1:{"state":11, "action":HALT_REUSE},
                2:{"state":12, "action":HALT_REUSE},
                3:{"state":13, "action":HALT_REUSE},
                4:{"state":21, "action":HALT_REUSE},
                5:{"state":5, "action":MOVE_NO_APPEND},
                6:{"state":16, "action":HALT_APPEND}
            },
            ":": {
                0:{"state":6, "action":MOVE_APPEND},
                1:{"state":11, "action":HALT_REUSE},
                2:{"state":12, "action":HALT_REUSE},
                3:{"state":13, "action":HALT_REUSE},
                4:{"state":21, "action":HALT_REUSE},
                5:{"state":5, "action":MOVE_NO_APPEND},
                6:{"state":-1, "action":ERROR}
            },
            ", ": {
                0:{"state":17, "action":HALT_APPEND},
                1:{"state":11, "action":HALT_REUSE},
                2:{"state":12, "action":HALT_REUSE},
                3:{"state":13, "action":HALT_REUSE},
                4:{"state":21, "action":HALT_REUSE},
                5:{"state":5, "action":MOVE_NO_APPEND},
                6:{"state":-1, "action":ERROR}
            },
            ";": {
                0:{"state":18, "action":HALT_APPEND},
                1:{"state":11, "action":HALT_REUSE},
                2:{"state":12, "action":HALT_REUSE},
                3:{"state":13, "action":HALT_REUSE},
                4:{"state":21, "action":HALT_REUSE},
                5:{"state":5, "action":MOVE_NO_APPEND},
                6:{"state":-1, "action":ERROR}
            },
            "(": {
                0:{"state":19, "action":HALT_APPEND},
                1:{"state":11, "action":HALT_REUSE},
                2:{"state":12, "action":HALT_REUSE},
                3:{"state":13, "action":HALT_REUSE},
                4:{"state":21, "action":HALT_REUSE},
                5:{"state":5, "action":MOVE_NO_APPEND},
                6:{"state":-1, "action":ERROR}
            },
            ")": {
                0:{"state":20, "action":HALT_APPEND},
                1:{"state":11, "action":HALT_REUSE},
                2:{"state":12, "action":HALT_REUSE},
                3:{"state":13, "action":HALT_REUSE},
                4:{"state":21, "action":HALT_REUSE},
                5:{"state":5, "action":MOVE_NO_APPEND},
                6:{"state":-1, "action":ERROR}
            },
            "_": {
                0:{"state":-1, "action":ERROR},
                1:{"state":1, "action":MOVE_APPEND},
                2:{"state":12, "action":HALT_REUSE},
                3:{"state":13, "action":HALT_REUSE},
                4:{"state":-1, "action":ERROR},
                5:{"state":5, "action":MOVE_NO_APPEND},
                6:{"state":-1, "action":ERROR}
            },
            "\t": {
                0:{"state":3, "action":MOVE_NO_APPEND},
                1:{"state":11, "action":HALT_NO_APPEND},
                2:{"state":12, "action":HALT_NO_APPEND},
                3:{"state":3, "action":MOVE_NO_APPEND},
                4:{"state":21, "action":HALT_NO_APPEND},
                5:{"state":5, "action":MOVE_NO_APPEND},
                6:{"state":-1, "action":ERROR}
            },
            "\n": {
                0:{"state":3, "action":MOVE_NO_APPEND},
                1:{"state":11, "action":HALT_NO_APPEND},
                2:{"state":12, "action":HALT_NO_APPEND},
                3:{"state":3, "action":MOVE_NO_APPEND},
                4:{"state":21, "action":HALT_NO_APPEND},
                5:{"state":15, "action":HALT_NO_APPEND},
                6:{"state":-1, "action":ERROR}
            },
            "?": {
                0:{"state":-1, "action":ERROR},
                1:{"state":-1, "action":ERROR},
                2:{"state":-1, "action":ERROR},
                3:{"state":-1, "action":ERROR},
                4:{"state":-1, "action":ERROR},
                5:{"state":5, "action":MOVE_NO_APPEND},
                6:{"state":-1, "action":ERROR}
            },
        }

        #Constant values for tokens
        #token code constants
        INT_LITERAL = 1
        EMPTY_SPACE = 0
        PLUS_OP = 3
        COMMENT = 4
        ASSIGN_OP = 5
        COMMA = 6
        SEMICOLON = 7
        LPAREN = 8
        RPAREN = 9
        MINUS_OP = 10
        BEGIN = 11
        END = 12
        READ = 13
        WRITE = 14
        ID = 15
        DOLLARSIGN = 16
        LAMBDA = 17


        #used in transition table to tranlsate final states to tokens

        TOKEN_CODES = {11: ID,
                       12: INT_LITERAL,
                       13: EMPTY_SPACE,
                       14: PLUS_OP,
                       15: COMMENT,
                       16: ASSIGN_OP,
                       17: COMMA,
                       18: SEMICOLON,
                       19: LPAREN,
                       20: RPAREN,
                       21: MINUS_OP,
                       22: BEGIN,
                       23: END,
                       24: READ,
                       25: WRITE
                      }

        #used for printing token codes
        TOKEN_FRIENDLY_TABLE = {
            INT_LITERAL:"IntLiteral",
            EMPTY_SPACE:"EmptySpace",
            PLUS_OP:"+",
            COMMENT:"Comment",
            ASSIGN_OP:":=",
            COMMA:",",
            SEMICOLON:";",
            LPAREN:"(",
            RPAREN:")",
            MINUS_OP:"-",
            BEGIN:"Begin",
            END:"End",
            READ:"Read",
            WRITE:"Write",
            ID:"Id",
            DOLLARSIGN:"$",
            LAMBDA:"Lambda"
        }

        EXCEPTION_TABLE = {
            "BEGIN":BEGIN,
            "END":END,
            "READ":READ,
            "WRITE":WRITE,
            "$":DOLLARSIGN,
            "LAMBDA":LAMBDA,
            "INTLITERAL":INT_LITERAL
        }

        def get_state_action_pair(curr_state, curr_char):
            """ Returns a pair representing current scanner state and the corresponding action """
            pair = [value for key, value in state_action_map.items() if curr_char in key]
            if len(pair) == 0:
                return state_action_map['?'][curr_state]
            else:
                return pair[0][curr_state]

        def lookup_code(curr_state, curr_char):
            """ Returns the token code generated by the final state curr_state """
            state_action_pair = get_state_action_pair(curr_state, curr_char)
            next_state = state_action_pair['state']
            return TOKEN_CODES[next_state]

        def check_exceptions(token_code, token_text):
            """ Checks token_text against the EXCEPTION_TABLE and modifies it accordingly """
            if token_text.upper() in EXCEPTION_TABLE:
                return EXCEPTION_TABLE[token_text.upper()]
            else:
                return token_code


        def scanner(line, terms, ndx=0, token_code=0, token_text=''):
            """ Scanner function """
            state = 0
            while ndx < len(line):
                state_action_pair = get_state_action_pair(state, line[ndx])
                next_action = state_action_pair['action']
                if next_action == ERROR:
                    print("lexical error")
                    exit()
                elif next_action == MOVE_APPEND:
                    state = state_action_pair['state']
                    token_text = token_text + line[ndx]
                    ndx += 1
                elif next_action == MOVE_NO_APPEND:
                    state = state_action_pair['state']
                    ndx += 1
                elif next_action == HALT_APPEND:
                    token_code = lookup_code(state, line[ndx])
                    token_text = token_text + line[ndx]
                    token_code = check_exceptions(token_code, token_text)
                    ndx += 1
                    if token_code == 0:
                        ret = scanner(line, terms, ndx, token_code, token_text)
                        ndx = ret["line position"]
                        token_code = ret["code"]
                    self.terminals.add(TOKEN_FRIENDLY_TABLE[token_code])
                    return {"code":token_code, "line position":ndx}
                elif next_action == HALT_NO_APPEND:
                    token_code = lookup_code(state, line[ndx])
                    token_code = check_exceptions(token_code, token_text)
                    ndx += 1
                    if token_code == 0:
                        ret = scanner(line, terms, ndx, token_code, token_text)
                        ndx = ret["line position"]
                        token_code = ret["code"]
                    self.terminals.add(TOKEN_FRIENDLY_TABLE[token_code])
                    return {"code":token_code, "line position":ndx}
                else:
                    token_code = lookup_code(state, line[ndx])
                    token_code = check_exceptions(token_code, token_text)
                    if token_code == 0:
                        ret = scanner(line, terms, ndx, token_code, token_text)
                        ndx = ret["line position"]
                        token_code = ret["code"]
                    self.terminals.add(TOKEN_FRIENDLY_TABLE[token_code])
                    return {"code":token_code, "line position":ndx}
            return {"code":None, "line position":ndx}

        new_rhs = set()
        for production in self.productions:
            rhs = production['rhs']
            matches = re.findall(r"<[\w ]+>", rhs)
            for match in matches:
                rhs = rhs.replace(match, "nonterminal")
            new_rhs.add(rhs)

        for line in new_rhs:
            ndx = 0
            while ndx < len(line):
                ret = scanner(line, self.terminals, ndx)
                ndx = ret["line position"]
                if ret["code"]:
                    self.terminals.add(TOKEN_FRIENDLY_TABLE[ret["code"]])
