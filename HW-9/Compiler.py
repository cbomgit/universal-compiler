import pdb
import re
from os import linesep

import Grammar
import Scanner
import TableGenerator
from RecTypes import *


class UCompiler:

    """
    Class defining Universal Compiler. Generates source code
    """
    def __init__(self, source_file_path, grammar_file_path):
        self.semantic_stack = []
        self.max_temp = 100
        self.obj_file = open("file.txt", "w")
        self.num_temps = 0
        self.parse_stack = []
        self.parse_stack_ndx = 0
        self.semantic_curr_ndx = 0
        self.input_file = source_file_path
        self.grammar_file = grammar_file_path
        self.symbol_table = []
        self.symbol_table_ndx = 0
        self.generated_code = ""

    def extract(self, semantic_record):
        """ extract the record value from the semantic record """
        if semantic_record.rec_type is SemanticRecordType.OpRec:
            return self.extract_op(semantic_record.record)
        elif semantic_record.rec_type is SemanticRecordType.ExprRec:
            return self.extract_rec(semantic_record.record)
        else:
            return None

    def extract_op(self, op_rec):
        """ Returns correct tuple prefix for math operation """
        if op_rec.op is OpType.PlusOp:
            return "ADD "
        else:
            return "SUB "

    def extract_rec(self, expr_rec):
        """ returns the value of the expression record """
        if expr_rec.kind is ExprType.IdExpr or expr_rec.kind is ExprType.TempExpr:
            return expr_rec.name
        else:
            return expr_rec.val

    def start(self):
        """ initializes compiler """
        self.max_temp = 0

    def finish(self):
        """ Writes the Halt instruction to generated code """
        self.generate("Halt")


    def generate(self, *args):
        """ function that writes code parameter to object code """
        num_args = len(args)
        arg_ndx = 1

        self.obj_file.write(args[0] + " ")
        self.generated_code = self.generated_code + args[0] + " "

        while arg_ndx < num_args:
            self.obj_file.write(args[arg_ndx] + ", ")
            self.generated_code = self.generated_code + args[arg_ndx] + ", "
            arg_ndx = arg_ndx + 1
        self.obj_file.write(linesep)
        self.generated_code = self.generated_code + linesep

    def look_up(self):
        """
        this can be compactly expressed using python syntax
        and is thus not implemented
        """
        print("look_up not implemented")

    def enter(self, string):
        """Enters string into the symbol table"""
        self.symbol_table.append(string)
        self.symbol_table_ndx = self.symbol_table_ndx + 1

    def check_id(self, id_string):
        """
        checks for id_string in the symbol table and enters it if not found.
        generates the appropriate object code for id_string
        """
        if id_string not in self.symbol_table:
            self.enter(id_string)
            self.generate("Declare", id_string, "Integer")

    def get_temp(self):
        """Gets the next temporary record available on the stack"""
        self.num_temps = self.num_temps + 1
        temp_name = "Temp&" + str(self.num_temps)
        self.check_id(temp_name)
        return temp_name

    def assign(self, target, source):
        """ Semantic route for #Assign action symbol"""
        self.generate("Store", source, target)

    def read_id(self, in_var):
        """ Semantic routine for #ReadId action symbol """
        self.generate("Read", in_var, "Integer")

    def write_expr(self, out_expr):
        """ Semantic routine for #WriteExpr action symbol """
        self.generate("Write", out_expr, "Integer")

    def gen_infix(self, e1, op, e2):
        """ write an expression to obj file using infix notation"""
        erec = self.get_temp()
        self.generate(op, e1, e2, erec)
        return erec

    def process_id(self, ndx):
        """ Processes an id record """
        self.check_id(self.semantic_stack[self.semantic_curr_ndx -1])
        result_rec = self.semantic_stack[self.semantic_curr_ndx]

    def process_literal(self, result_rec):
        """ Processes a literal record """
        result_rec = SemanticRecord(SemanticRecordType.ExprRec, ExprType.LiteralExpr)
        result_rec.record.val = int(self.semantic_stack[self.semantic_curr_ndx - 1])

    def process_op(self, result_rec):
        """ Processes an operator record """
        operator = self.semantic_stack[self.semantic_curr_ndx - 1]
        result_rec = SemanticRecord(SemanticRecordType.OpRec, operator)

    def copy(self, source, target):
        target = source


    def compile(self):
        scanner = Scanner.MicroScanner(self.input_file)
        grammar = Grammar.Grammar(self.grammar_file)
        predict = TableGenerator.TableGenerator(grammar)

        self.semantic_stack.append(grammar.start_symbol)
        self.parse_stack.append(grammar.start_symbol)
        left_ndx = right_ndx = 0
        curr_ndx = 1
        top_ndx = 2

        current_token = scanner.scan()
        while len(self.semantic_stack) != 0 and len(self.parse_stack) != 0:
            
            #print info to the console
            remaining_input = (scanner.token_text.strip('\n') + " " + scanner.get_remaining_input()).strip('\n')
            print("Remaining Input: " + remaining_input)
            print("Parse Stack: " + str(self.parse_stack))
            print("Semantic Stack: " + str(self.semantic_stack))
            print("Generated Code")
            print(self.generated_code)
            print(linesep)
            print(linesep)

            x = self.parse_stack[len(self.parse_stack) - 1]
            if isinstance(x, dict):
                left_ndx = x['left']
                right_ndx = x['right']
                curr_ndx = x['curr']
                top_ndx = x['top']
                curr_ndx = curr_ndx + 1
                self.parse_stack.pop()
            elif x in grammar.non_terminals:
                if predict.table[x][scanner.TOKEN_FRIENDLY_TABLE[current_token]] is not None:
                    self.parse_stack.pop()

                    eop = {"left":left_ndx, "right":right_ndx, "curr":curr_ndx, "top":top_ndx}
                    self.parse_stack.append(eop)
                    prod_num = predict.table[x][scanner.TOKEN_FRIENDLY_TABLE[current_token]]
                    rhs = grammar.productions[prod_num]['rhs']

                    all_syms = grammar.action_symbols | grammar.non_terminals | grammar.terminals
                    symbols = grammar.symbols_in_string(rhs, all_syms)
                    ndx = len(symbols) - 1
                    while ndx > -1:
                        if symbols[ndx] != 'Lambda':
                            self.parse_stack.append(symbols[ndx])
                        if symbols[ndx] not in grammar.action_symbols:
                            self.semantic_stack.append(symbols[ndx])
                        ndx = ndx - 1
                        left_ndx = curr_ndx
                        right_ndx = top_ndx
                        curr_ndx = right_ndx
                        top_ndx = top_ndx + len(set(symbols).difference(grammar.action_symbols))
                else:
                    print("Syntax error: Symbol " + x + " Token " + current_token)
                    exit()
            elif x in grammar.terminals:
                if x == scanner.TOKEN_FRIENDLY_TABLE[current_token]:
                    self.semantic_stack.append(scanner.token_text)
                    self.parse_stack.pop()
                    current_token = scanner.scan()
                else:
                    print("Syntax error: non match: " + str(x) + " and " + str(current_token))
                    exit()
            else:
                self.parse_stack.pop()
                if x == "#Start":
                    self.start()
                elif x == "#Assign{1 3}":
                    target = self.semantic_stack[len(self.semantic_stack) - 2]
                    source = self.semantic_stack[len(self.semantic_stack) - 4]
                    self.assign(target, source)
                elif x == "#ReadId{1}":
                    invar = self.semantic_stack[len(self.semantic_stack) - 2]
                    self.read_id(invar)
                elif x == "#WriteExpr{1}":
                    outvar = self.semantic_stack[len(self.semantic_stack) - 2]
                    self.write_expr(outvar)
                elif x.find("Copy") == 0:
                    args = re.findall(r"\d", x)
                    arg1 = int(args[0])
                    arg2 = int(args[1])
                    top = len(self.semantic_stack) - 1
                    self.semantic_stack[top - arg1] = self.semantic_stack[top - arg2]
                elif x == "#GenInfix{0 1 2 0}":
                    pdb.set_trace()
                    e1 = self.semantic_stack[len(self.semantic_stack) - 1]
                    op = self.semantic_stack[len(self.semantic_stack) - 2]
                    e2 = self.semantic_stack[len(self.semantic_stack) - 3]
                    result = self.gen_infix(e1, op, e2)
                    self.semantic_stack[len(self.semantic_stack) - 1] = result
                elif x == "#ProcessLiteral{0}":
                    print("Process literal")
                elif x == "#ProcessOp{0}":
                    print("Process op")
                elif x == "#ProcessId{0}":
                    self.check_id(self.semantic_stack[len(self.semantic_stack) - 1])
                elif x == "#Finish":
                    self.finish()

        self.obj_file.close()