from enum import Enum

class SemanticRecordType(Enum):
    """ Enum definig Semantic record types """
    OpRec = 1
    ExprRec = 2
    ErrorRec = 3

class ExprType(Enum):
    """ Enum defining expression record types """
    IdExpr = 1
    LiteralExpr = 2
    TempExpr = 3

class OpType(Enum):
    """ Enum definig operator record types """
    PlusOp = 1
    MinusOp = 2

class SemanticRecord:

    """ Class defining a semantic record """
    def __init__(self, rec_type, inner_record_type=None):
        self.rec_type = rec_type
        if self.rec_type is SemanticRecordType.OpRec:
            self.record = OpRec(inner_record_type)
        elif self.rec_type is SemanticRecordType.ExprRec:
            self.record = ExprRec(inner_record_type)
        else:
            self.record = None

    def get_data(self):
        """ Returns the record data in this semantic record """
        if self.rec_type is SemanticRecordType.OpRec:
            return self.record

class OpRec:
    """ Describes an operator record as PlusOp or MinusOp """
    def __init__(self, op_type):
        self.operator = op_type

class ExprRec:
    """ Describes an Expression record """

    def __init__(self, expr_type):
        self.kind = expr_type

        if expr_type is ExprType.IdExpr or expr_type is ExprType.TempExpr:
            self.name = ""
        else:
            self.val = 0
            