import Scanner
import Grammar
import TableGenerator
import pdb


"""
file_name = input("What is the file name: ")
scanner = MicroScanner(file_name)
token_code = ''
pdb.set_trace()
while token_code != MicroScanner.END:
    token_code = scanner.next_token()
    print(MicroScanner.TOKEN_FRIENDLY_TABLE[token_code])

scanner.file.close()
print(scanner.TOKEN_FRIENDLY_TABLE[scanner.EOFSYM])
"""

source_file_name = 'program.txt'
scanner = Scanner.MicroScanner(source_file_name)

productions_file_name = 'productions.txt'
grammar = Grammar.Grammar('productions.txt')
predict_table = TableGenerator.TableGenerator(grammar).table

stack = []

start_symbol = grammar.productions[grammar.start_symbol]['lhs']

stack.append(start_symbol)

current_symbol =  start_symbol
current_token = scanner.next_token()
#pdb.set_trace()
while len(stack) > 0:
    current_symbol = stack[len(stack) - 1]
    if current_symbol in grammar.non_terminals:
        if predict_table[current_symbol][scanner.TOKEN_FRIENDLY_TABLE[current_token]] is not None:
            predict_num = predict_table[current_symbol][scanner.TOKEN_FRIENDLY_TABLE[current_token]]
            rhs = grammar.productions[predict_table[current_symbol][scanner.TOKEN_FRIENDLY_TABLE[current_token]]]['rhs']
            symbols = grammar.symbols_in_string(rhs, grammar.symbols)
            ndx = len(symbols) - 1
            stack.pop()
            while ndx > -1:
                if symbols[ndx] != 'Lambda':
                    stack.append(symbols[ndx])
                ndx = ndx - 1
            print(predict_num, scanner.get_remaining_input(), stack)
        else:
            print("1 - Syntax error: symbol: %s token: %s") % (current_symbol, scanner.TOKEN_FRIENDLY_TABLE[current_token])
            exit()
    elif scanner.TOKEN_FRIENDLY_TABLE[current_token] == current_symbol:
        print("Match", scanner.get_remaining_input(), stack)
        stack.pop()
        current_token = scanner.next_token()
    else:
        print("2 - syntax error: symbol: %s token: %s") % (current_symbol, scanner.TOKEN_FRIENDLY_TABLE[current_token])
        exit()
