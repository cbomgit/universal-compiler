import pdb
from os import linesep
import Grammar
import Scanner
import TableGenerator

source_file_name = input("Path to source code: ")
scanner = Scanner.MicroScanner(source_file_name)

productions_file_name = 'productions.txt'
grammar = Grammar.Grammar(productions_file_name)
predict_table = TableGenerator.TableGenerator(grammar).table

stack = []

start_symbol = grammar.productions[grammar.start_symbol]['lhs']

stack.append(start_symbol)

current_symbol =  start_symbol
current_token = scanner.scan()
#pdb.set_trace()
print("{:<50s}{:<50s}{:>70s}".format("Parser Action", "Remaining Input", "Parse Stack"))
while len(stack) > 0:
    current_symbol = stack[len(stack) - 1]
    if current_symbol in grammar.non_terminals:
        if predict_table[current_symbol][scanner.TOKEN_FRIENDLY_TABLE[current_token]] is not None:
            predict_num = predict_table[current_symbol][scanner.TOKEN_FRIENDLY_TABLE[current_token]]
            rhs = grammar.productions[predict_table[current_symbol][scanner.TOKEN_FRIENDLY_TABLE[current_token]]]['rhs']
            symbols = grammar.symbols_in_string(rhs, grammar.symbols)
            ndx = len(symbols) - 1
            stack.pop()
            while ndx > -1:
                if symbols[ndx] != 'Lambda':
                    stack.append(symbols[ndx])
                ndx = ndx - 1
            remaining_input = (scanner.token_text.strip('\n') + " " + scanner.get_remaining_input()).strip('\n')
            print("{:<50s}{:<50s}{:>70s}".format(str(predict_num), remaining_input, str(stack)))
        else:
            print("1 - Syntax error: symbol: %s token: %s") % (current_symbol, scanner.TOKEN_FRIENDLY_TABLE[current_token])
            exit()
    elif scanner.TOKEN_FRIENDLY_TABLE[current_token] == current_symbol:
        remaining_input = (scanner.token_text.strip('\n') + " " + scanner.get_remaining_input()).strip('\n')
        print("{:<50s}{:<50s}{:>70s}".format("Match", remaining_input, str(stack)))
        stack.pop()
        current_token = scanner.scan()
    else:
        print("2 - syntax error: symbol: %s token: %s") % (current_symbol, scanner.TOKEN_FRIENDLY_TABLE[current_token])
        exit()
