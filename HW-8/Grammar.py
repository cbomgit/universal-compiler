import re
import pdb
from sys import exit
import Scanner

class Grammar:
    """ A class representing a grammar """

    #filename is the path to a file containg the list of productions describing this grammar

    def __init__(self, filename):

        """ Productions is an array of dicts of the form :
            { lhs: 'left hand side of production',
              rhs: 'right hand side of production',
              predict: set()
            }
        """

        self.productions = []

        #terminal symbols in this grammar
        self.terminals = set()

        #non terminal symbols in this grammar
        self.non_terminals = set()

        #all symbols in this grammar (union of terminal and non-terminal sets)
        self.symbols = set()

        #first sets for all symbols
        self.first_sets = {}

        #follow sets for all symbols
        self.follow_sets = {}

        #predict sets for all symbols - {'symbol': set()}
        self.predict_sets = {}

        self.action_symbols = set() 
        #symbols found to derive lambda are marked as true in this set,
        #otherwise they are marked as false
        # {'symbol':True or False}
        self.derives_lambda = {}

        self.read_productions(filename)
        self.symbols = self.terminals | self.non_terminals
        self.mark_lambda()
        self.fill_first()
        self.fill_follow()
        self.generate_predict()

    
    def production_nonterm_goes_to_term(self, non_terminal, terminal):
        for production in self.productions:
            if production['lhs'] == non_terminal and production['rhs'].find(terminal) == 0:
                return True
        return False
                
        
    def mark_lambda(self):
        changes = True
        rhs_derives_lambda = True

        for symbol in self.symbols:
            self.derives_lambda[symbol] = False

        while changes:
            changes = False
            for production in self.productions:
                rhs_derives_lambda = True
                symbols_in_rhs = [x for x in self.symbols if x in production['rhs']]
                for symbol in symbols_in_rhs:
                    rhs_derives_lambda = rhs_derives_lambda and symbol == 'Lambda'
                if rhs_derives_lambda and not self.derives_lambda[production['lhs']]:
                    changes = True
                    self.derives_lambda[production['lhs']] = True

    def non_term_in_rhs(self, non_term):
        result = []
        for p in self.productions:
            if p['rhs'].find(non_term) != -1:
                result.append(p)

    def symbols_in_string(self, string_of_symbols, valid_symbols):
        index_tuples = []
        for sym in valid_symbols:
            start_index = string_of_symbols.find(sym)
            if start_index > -1:
                tup = (start_index, start_index + len(sym))
                index_tuples.append(tup)
        index_tuples.sort()
        
        found_symbols = []
        for tup in index_tuples:
            found_symbol = string_of_symbols[tup[0]:tup[1]]
            found_symbols.append(found_symbol)
        return found_symbols

    def compute_first(self, string):
        symbols = self.symbols_in_string(string, self.symbols)
        k = len(symbols)
        result = set()
        if k == 0:
            result.add('Lambda')
        else:
            result = self.first_sets[symbols[0]].copy() - {'Lambda'}
            i = 0
            while i < k and 'Lambda' in self.first_sets[symbols[i]]:
                result = result | (self.first_sets[symbols[i]] - {'Lambda'})
                i = i + 1
            if i == k and 'Lambda' in self.first_sets[symbols[k - 1]]:
                result = result | {'Lambda'}
        return result

        
    def fill_first(self):
        for non_term in self.non_terminals:
            if self.derives_lambda[non_term]:
                self.first_sets[non_term] = set({'Lambda'})
            else:
                self.first_sets[non_term] = set()
        for term in self.terminals:
            self.first_sets[term] = set({term})
            for non_term in self.non_terminals:
               if self.production_nonterm_goes_to_term(non_term, term):
                   self.first_sets[non_term] = self.first_sets[non_term] | set({term})
        changes = True
        while changes:
            changed_sets = 0
            for production in self.productions:
                temp_first = self.first_sets[production['lhs']].copy()
                self.first_sets[production['lhs']] = self.first_sets[production['lhs']] | self.compute_first(production['rhs'])
                if temp_first != self.first_sets[production['lhs']]:
                    changed_sets = changed_sets + 1
            if changed_sets == 0:
                changes = False


    def fill_follow(self):
        for lhs_nonterms in self.non_terminals:
            self.follow_sets[lhs_nonterms] = set()
        self.follow_sets[self.productions[self.start_symbol]['lhs']] = set({'Lambda'})
        changes = True
        while changes:
            changed_sets = 0
            for n in self.non_terminals:
                productions = self.non_term_in_rhs(n)
                for p in self.productions:
                    rhs = p['rhs']
                    n_locations = [(m.start(), m.end()) for m in re.finditer(n, rhs)]
                    for tup in n_locations:
                        y = rhs[tup[1]:].strip(" ")
                        temp_follow = self.follow_sets[n].copy()
                        self.follow_sets[n] = self.follow_sets[n] | (self.compute_first(y) - {'Lambda'})
                        if 'Lambda' in self.compute_first(y):
                            self.follow_sets[n] = self.follow_sets[n] | self.follow_sets[p['lhs']]
                        if temp_follow != self.follow_sets[n]:
                            changed_sets = changed_sets + 1
            if changed_sets == 0:
                changes = False

    def generate_predict(self):
        for production in self.productions:
            symbols_in_rhs = self.symbols_in_string(production['rhs'], self.symbols)
            first_symbol = symbols_in_rhs[0]
            if 'Lambda' in self.first_sets[first_symbol]:
                production['predict'] = (self.first_sets[first_symbol] | self.follow_sets[production['lhs']]) - {'Lambda'}
            else:
                production['predict'] = self.first_sets[first_symbol]


    def read_productions(self, f):
        """ Breaks down the grammar and populates the data objects created
            in the constructor
        """
        #get all productions and non-terminals
        f = open("productions.txt", "r")
        i = 0
        tokens = Scanner.MicroScanner.TOKEN_FRIENDLY_TABLE.values()
        for line in f:

            #split into lhs and rhs
            rhs = line.split(" -> ")[1]
            lhs= line.split(" -> ")[0]
            self.productions.append({"lhs":lhs,"rhs":rhs})

            #extract the action symbols first from rhs and replace it
            #with the empty string
            action_symbol = re.findall(r"#\w*\{[\!0-9, ]+\}|#\w*", rhs)
            if len(action_symbol) > 0:
                for sym in action_symbol:
                    self.action_symbols.add(sym)
                    rhs = rhs.replace(sym, '').strip(' ')

            matches = re.findall(r"<[\w ]+>", rhs)
            for m in matches:
                rhs = rhs.replace(m, '')

            for t in tokens:
                if rhs.find(t) > -1:
                    self.terminals.add(t)

            self.non_terminals.add(lhs)
            if rhs.find("$") != -1:
                self.start_symbol = i
            i = i + 1
        f.close()
