import Grammar
import pdb


class TableGenerator:

        #Constant values for tokens
    #token code constants
    INT_LITERAL = 1
    EMPTY_SPACE = 0
    PLUS_OP = 3
    COMMENT = 4
    ASSIGN_OP = 5
    COMMA = 6
    SEMICOLON = 7
    LPAREN = 8
    RPAREN = 9
    MINUS_OP = 10
    BEGIN = 11
    END = 12
    READ = 13
    WRITE = 14
    ID = 15
    EOFSYM = 16

    #used for printing token codes
    token_friendly_table = {
        INT_LITERAL:"INT_LITERAL",
        EMPTY_SPACE:"EMPTYSPACE",
        PLUS_OP:"PLUS_OP",
        COMMENT:"COMMENT",
        ASSIGN_OP:"ASSIGNOP",
        COMMA:"COMMA",
        SEMICOLON:"SEMICOLON",
        LPAREN:"LPAREN",
        RPAREN:"RPAREN",
        MINUS_OP:"MINUSOP",
        BEGIN:"BEGINSYM",
        END:"ENDSYM",
        READ:"READSYM",
        WRITE:"WRITESYM",
        ID:"ID",
        EOFSYM:"EoF"
    }

    def __init__(self, grammar):
        self.table = self.generate_table(grammar)


    def generate_table(self, grammar):
        table = {}
        ndx = 0
        terms = grammar.terminals - {'Lambda'}
        #productions that do not derive Lambda

        for prd in grammar.productions:
            table[prd['lhs']] = {}
            for t in grammar.terminals:
                table[prd['lhs']][t] = None

        for prd in grammar.productions:
            for terminal in prd['predict']:
                table[prd['lhs']][terminal] = ndx
            ndx = ndx + 1

        return table


        