#!/usr/bin/env python
#action constants
MOVE_APPEND = "MoveAppend"
MOVE_NO_APPEND = "MoveNoAppend"
HALT_APPEND = "HaltAppend"
HALT_NO_APPEND = "HaltNoAppend"
HALT_REUSE = "HaltReuse"
ERROR = "Error"

state_action_map = { 
            "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ": {
                0:{"state":1, "action":MOVE_APPEND },
                1:{"state":1, "action":MOVE_APPEND},
                2:{"state":12,"action":HALT_REUSE},
                3:{"state":13,"action":HALT_REUSE},
                4:{"state":21,"action":HALT_REUSE},
                5:{"state":5, "action":MOVE_NO_APPEND},
                6:{"state":-1,"action":ERROR}
            },
            "0123456789": {
                0:{"state":2, "action":MOVE_APPEND},
                1:{"state":1, "action":MOVE_APPEND},
                2:{"state":2, "action":MOVE_APPEND},
                3:{"state":13,"action":HALT_REUSE},
                4:{"state":21,"action":HALT_REUSE},
                5:{"state":5, "action":MOVE_NO_APPEND},
                6:{"state":-1,"action":ERROR}
            },
            " ": {
                0:{"state":3, "action":MOVE_NO_APPEND},
                1:{"state":11,"action":HALT_NO_APPEND},
                2:{"state":12,"action":HALT_NO_APPEND},
                3:{"state":3, "action":MOVE_NO_APPEND},
                4:{"state":21,"action":HALT_NO_APPEND},
                5:{"state":5, "action":MOVE_NO_APPEND},
                6:{"state":-1,"action":ERROR}
            },
            "+": {
                0:{"state":14,"action":HALT_NO_APPEND},
                1:{"state":11,"action":HALT_REUSE},
                2:{"state":12,"action":HALT_REUSE},
                3:{"state":13,"action":HALT_REUSE},
                4:{"state":21,"action":HALT_REUSE},
                5:{"state":5, "action":MOVE_NO_APPEND},
                6:{"state":-1,"action":ERROR}
            },
            "-": {
                0:{"state":4, "action":MOVE_APPEND},
                1:{"state":11,"action":HALT_REUSE},
                2:{"state":12,"action":HALT_REUSE},
                3:{"state":13,"action":HALT_REUSE},
                4:{"state":5, "action":MOVE_NO_APPEND},
                5:{"state":5, "action":MOVE_NO_APPEND},
                6:{"state":-1,"action":ERROR}
            },
            "=": {
                0:{"state":-1,"action":ERROR},
                1:{"state":11,"action":HALT_REUSE},
                2:{"state":12,"action":HALT_REUSE},
                3:{"state":13,"action":HALT_REUSE},
                4:{"state":21,"action":HALT_REUSE},
                5:{"state":5, "action":MOVE_NO_APPEND},
                6:{"state":16,"action":HALT_APPEND}
            },
            ":": {
                0:{"state":6,"action":MOVE_APPEND},
                1:{"state":11,"action":HALT_REUSE},
                2:{"state":12,"action":HALT_REUSE},
                3:{"state":13,"action":HALT_REUSE},
                4:{"state":21,"action":HALT_REUSE},
                5:{"state":5, "action":MOVE_NO_APPEND},
                6:{"state":-1,"action":ERROR}
            },
            ",": {
                0:{"state":17,"action":HALT_APPEND},
                1:{"state":11,"action":HALT_REUSE},
                2:{"state":12,"action":HALT_REUSE},
                3:{"state":13,"action":HALT_REUSE},
                4:{"state":21,"action":HALT_REUSE},
                5:{"state":5, "action":MOVE_NO_APPEND},
                6:{"state":-1,"action":ERROR}
            },
            ";": {
                0:{"state":18,"action":HALT_APPEND},
                1:{"state":11,"action":HALT_REUSE},
                2:{"state":12,"action":HALT_REUSE},
                3:{"state":13,"action":HALT_REUSE},
                4:{"state":21,"action":HALT_REUSE},
                5:{"state":5, "action":MOVE_NO_APPEND},
                6:{"state":-1,"action":ERROR}
            },
            "(": {
                0:{"state":19,"action":HALT_APPEND},
                1:{"state":11,"action":HALT_REUSE},
                2:{"state":12,"action":HALT_REUSE},
                3:{"state":13,"action":HALT_REUSE},
                4:{"state":21,"action":HALT_REUSE},
                5:{"state":5, "action":MOVE_NO_APPEND},
                6:{"state":-1,"action":ERROR}
            },
            ")": {
                0:{"state":20,"action":HALT_APPEND},
                1:{"state":11,"action":HALT_REUSE},
                2:{"state":12,"action":HALT_REUSE},
                3:{"state":13,"action":HALT_REUSE},
                4:{"state":21,"action":HALT_REUSE},
                5:{"state":5, "action":MOVE_NO_APPEND},
                6:{"state":-1,"action":ERROR}
            },
            "_": {
                0:{"state":-1,"action":ERROR},
                1:{"state":1, "action":MOVE_APPEND},
                2:{"state":12,"action":HALT_REUSE},
                3:{"state":13,"action":HALT_REUSE},
                4:{"state":-1,"action":ERROR},
                5:{"state":5, "action":MOVE_NO_APPEND},
                6:{"state":-1,"action":ERROR}
            },
            "\t": {
                0:{"state":3, "action":MOVE_NO_APPEND},
                1:{"state":11,"action":HALT_NO_APPEND},
                2:{"state":12,"action":HALT_NO_APPEND},
                3:{"state":3, "action":MOVE_NO_APPEND},
                4:{"state":21,"action":HALT_NO_APPEND},
                5:{"state":5, "action":MOVE_NO_APPEND},
                6:{"state":-1,"action":ERROR}
            },
            "\n": {
                0:{"state":3, "action":MOVE_NO_APPEND},
                1:{"state":11,"action":HALT_NO_APPEND},
                2:{"state":12,"action":HALT_NO_APPEND},
                3:{"state":3, "action":MOVE_NO_APPEND},
                4:{"state":21,"action":HALT_NO_APPEND},
                5:{"state":15,"action":HALT_NO_APPEND},
                6:{"state":-1,"action":ERROR}
            },
            "?": {
                0:{"state":-1,"action":ERROR},
                1:{"state":-1,"action":ERROR},
                2:{"state":-1,"action":ERROR},
                3:{"state":-1,"action":ERROR},
                4:{"state":-1,"action":ERROR},
                5:{"state":5, "action":MOVE_NO_APPEND},
                6:{"state":-1,"action":ERROR}
            },
}

#Constant values for tokens
#token code constants
INT_LITERAL = 1
EMPTY_SPACE = 0
PLUS_OP = 3
COMMENT = 4
ASSIGN_OP = 5
COMMA = 6
SEMICOLON = 7
LPAREN = 8
RPAREN = 9
MINUS_OP = 10
BEGIN = 11
END = 12
READ = 13
WRITE = 14
ID = 15
EOFSYM = 16


#used in transition table to tranlsate final states to tokens

token_codes = { 11: ID,
                12: INT_LITERAL,
                13: EMPTY_SPACE,
                14: PLUS_OP,
                15: COMMENT,
                16: ASSIGN_OP,
                17: COMMA,
                18: SEMICOLON,
                19: LPAREN,
                20: RPAREN,
                21: MINUS_OP,
                22: BEGIN,
                23: END,
                24: READ,
                25: WRITE
}

#used for printing token codes
token_friendly_table = {
    INT_LITERAL:"INT_LITERAL",
    EMPTY_SPACE:"EMPTYSPACE",
    PLUS_OP:"PLUS_OP",
    COMMENT:"COMMENT",
    ASSIGN_OP:"ASSIGNOP",
    COMMA:"COMMA",
    SEMICOLON:"SEMICOLON",
    LPAREN:"LPAREN",
    RPAREN:"RPAREN",
    MINUS_OP:"MINUSOP",
    BEGIN:"BEGINSYM",
    END:"ENDSYM",
    READ:"READSYM",
    WRITE:"WRITESYM",
    ID:"ID",
    EOFSYM:"EoF"
}

exception_table = {
    
    "BEGIN":BEGIN,
    "END":END,
    "READ":READ,
    "WRITE":WRITE,
}

def current_char(f):
    return_char = f.read(1)
    f.seek(f.tell() - 1)
    return return_char


def consume_char(f):
    return f.read(1)

     
def get_state_action_pair(curr_state, curr_char):
    
    pair = [value for key, value in state_action_map.items() if curr_char in key]
    if len(pair) == 0:
        return state_action_map['?'][curr_state]
    else:
        return pair[0][curr_state]
        
def lookup_code(curr_state, curr_char):
    
    state_action_pair = get_state_action_pair(curr_state, curr_char)
    next_state = state_action_pair['state']
    return token_codes[next_state]

def check_exceptions(token_code, token_text):
    if token_text.upper() in exception_table:
        return exception_table[token_text.upper()]
    else:
        return token_code
        
def lexical_error(f, state, token_code = 0, token_text=''):
    print("lexical error")
    exit()
    
def move_append(f, state, token_code=0, token_text=''):
   state = get_state_action_pair(state, current_char(f))['state']
   token_text+=current_char(f)
   consume_char(f)
   return {"state":state, "code":None, "text":token_text}
   
def move_no_append(f, state, token_code=0, token_text=''):
   state = get_state_action_pair(state,current_char(f))['state']
   consume_char(f)
   return {"state":state, "code":None, "text":token_text}
   
def halt_append(f, state, token_code = 0, token_text=''):
    token_code = lookup_code(state, current_char(f))
    token_text += current_char(f)
    token_code = check_exceptions(token_code, token_text)
    consume_char(f)
    if token_code == 0:
        token_code = scanner(f, token_code, token_text)
    return {"state":state, "code":token_code, "text":token_text}

def halt_no_append(f, state, token_code = 0, token_text=''):
    token_code = lookup_code(state, current_char(f))
    token_code = check_exceptions(token_code, token_text)
    consume_char(f)
    if token_code == 0:
        token_code = scanner(f, token_code, token_text)
    return {"state":state, "code":token_code, "text":token_text}

def halt_reuse(f, state, token_code = 0, token_text=''):
    token_code = lookup_code(state, current_char(f))
    token_code = check_exceptions(token_code, token_text)
    if token_code == 0:
        token_code = scanner(f, token_code, token_text)
    return {"state":state, "code":token_code, "text":token_text}
    
    
action_table = {
    ERROR:lexical_error,
    MOVE_APPEND:move_append,
    MOVE_NO_APPEND:move_no_append,
    HALT_APPEND: halt_append,
    HALT_NO_APPEND:halt_no_append,
    HALT_REUSE:halt_reuse
}
    
def scanner(f, token_code = 0, token_text = ''):
    state = 0
    while current_char(f) != '':
        return_val = action_table[get_state_action_pair(state, current_char(f))['action']](f, state, token_code, token_text)
        token_text = return_val['text']
        state = return_val["state"]
        if return_val["code"]:
            return return_val["code"]
    return END


file_name = input("What is the file name: ")
f = open(file_name, 'r')
token_code = ''
while token_code != END:
    token_code = scanner(f)
    print(token_friendly_table[token_code])

print(token_friendly_table[EOFSYM])