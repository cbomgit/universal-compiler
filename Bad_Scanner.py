#!/usr/bin/env python

import sys

def current_char(f):
    return_char = f.read(1)
    f.seek(f.tell() - 1)
    return return_char


def consume_char(f):
    return f.read(1)

def next_state(state, char):
    if char.isalpha():
        return state_table[state][0]
    elif char.isdigit():
        return state_table[state][1]
    elif char == " ":
        return state_table[state][2]
    elif char == "+":
        return state_table[state][3]
    elif char == "-":
        return state_table[state][4]
    elif char == "=":
        return state_table[state][5]
    elif char == ":":
        return state_table[state][6]
    elif char == ",":
        return state_table[state][7]
    elif char == ";":
        return state_table[state][8]
    elif char == "(":
        return state_table[state][9]
    elif char == ")":
        return state_table[state][10]
    elif char == "_":
        return state_table[state][11]
    elif char == "\t":
        return state_table[state][12]
    elif char == "\n":
        return state_table[state][13]
    else:
        return " "

def action(state, char):
    if char.isalpha():
        return action_table[state][0]
    elif char.isdigit():
        return action_table[state][1]
    elif char == " ":
        return action_table[state][2]
    elif char == "+":
        return action_table[state][3]
    elif char == "-":
        return action_table[state][4]
    elif char == "=":
        return action_table[state][5]
    elif char == ":":
        return action_table[state][6]
    elif char == ",":
        return action_table[state][7]
    elif char == ";":
        return action_table[state][8]
    elif char == "(":
        return action_table[state][9]
    elif char == ")":
        return action_table[state][10]
    elif char == "_":
        return action_table[state][11]
    elif char == "\t":
        return action_table[state][12]
    elif char == "\n":
        return action_table[state][13]
    else:
        return action_table[state][14]
#                     L   D   B   +   -  =    :  ,    ;   (   )  _   Tab Eol  Other
state_table = { 0:  [ 1,  2,  3, 14,  4," ",  6, 17, 18, 19, 20," ",  3,  3, " "],
                1:  [ 1,  1, 11, 11, 11, 11, 11, 11, 11, 11, 11,  1, 11, 11, " "],
                2:  [12,  2, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, " "],
                3:  [13, 13,  3, 13, 13, 13, 13, 13, 13, 13, 13, 13,  3,  3, " "],
                4:  [21, 21, 21, 21,  5, 21, 21, 21, 21, 21, 21," ", 21, 21, " "],
                5:  [ 5,  5,  5,  5,  5,  5,  5,  5,  5,  5,  5,  5,  5, 15,   5],
                6:  [" ", " ", " ", " ", " ", 16, " ", " ", " ", " ", " ", " ", " ", " ", " "],
}

#token code constants
INT_LITERAL = 1
EMPTY_SPACE = 0
PLUS_OP = 3
COMMENT = 0 
ASSIGN_OP = 5
COMMA = 6
SEMICOLON = 7
LPAREN = 8
RPAREN = 9
MINUS_OP = 10
BEGIN = 11
END = 12
READ = 13
WRITE = 14
ID = 15

#used for printing token codes
token_friendly_table = {
    INT_LITERAL:"INT_LITERAL",
    EMPTY_SPACE:"EMPTYSPACE",
    PLUS_OP:"PLUS_OP",
    COMMENT:"COMMENT",
    ASSIGN_OP:"ASSIGNOP",
    COMMA:"COMMA",
    SEMICOLON:"SEMICOLON",
    LPAREN:"LPAREN",
    RPAREN:"RPAREN",
    MINUS_OP:"MINUSOP",
    BEGIN:"BEGINSYM",
    END:"ENDSYM",
    READ:"READSYM",
    WRITE:"WRITESYM",
    ID:"ID"
}
#used in transition table to tranlsate final states to tokens
token_codes = { 11: ID,
                12: INT_LITERAL,
                13: EMPTY_SPACE,
                14: PLUS_OP,
                15: COMMENT,
                16: ASSIGN_OP,
                17: COMMA,
                18: SEMICOLON,
                19: LPAREN,
                20: RPAREN,
                21: MINUS_OP,
                22: BEGIN,
                23: END,
                24: READ,
                25: WRITE
}

#action codes
ER = 0
MA = 1
MN = 2
HA = 3
HN = 4
HR = 5

#action table        L   D   B   +   -    =   :   ,  ;   (    )   _  \t  \n  ?
action_table = { 0: [MA, MA, MN, HA, MA, ER, MA, HA, HA, HA, HA, ER, MN, MN, ER],
                 1: [MA, MA, HN, HR, HR, HR, HR, HR, HR, HR, HR, MA, HN, HN, ER],
                 2: [HR, MA, HN, HR, HR, HR, HR, HR, HR, HR, HR, HR, HN, HN, ER],
                 3: [HR, HR, MN, HR, HR, HR, HR, HR, HR, HR, HR, HR, MN, MN, ER],
                 4: [HR, HR, HN, HR, MN, HR, HR, HR, HR, HR, HR, ER, HN, HN, ER],
                 5: [MN, MN, MN, MN, MN, MN, MN, MN, MN, MN, MN, MN, MN, HN, MN],
                 6: [ER, ER, ER, ER, ER, HA, ER, ER, ER, ER, ER, ER, ER, ER, ER]
}




def lookup_code(state, current_char):
    next = next_state(state, current_char)
    return token_codes[next]

def check_exceptions(token_code, token_text):
    if token_text == "BEGIN":
        return BEGIN;
    elif token_text == "END":
        return END;
    elif token_text == "READ":
        return READ;
    elif token_text == "WRITE":
        return WRITE;
    else:
        return token_code

def scanner(token_code = 0, token_text = ''):
    state = 0
    while True:
        next_action = action(state, current_char(f))
        #print("State: " + str(state) + " char: " + current_char(f) + " = action: " + str(next_action))
        if next_action == ER:
            print("lexical error")
            exit()
        elif next_action == MA:
            state = next_state(state,current_char(f))
            token_text = token_text + current_char(f)
            consume_char(f)
        elif next_action == MN:
            state = next_state(state, current_char(f))
            consume_char(f)
        elif next_action == HA:
            token_code = lookup_code(state, current_char(f))
            token_text = token_text + current_char(f)
            token_code = check_exceptions(token_code, token_text)
            consume_char(f)
            if token_code == 0:
                token_code = scanner(token_code, token_text)
            return token_code
        elif next_action == HN:
            token_code = lookup_code(state, current_char(f))
            token_code = check_exceptions(token_code, token_text)
            consume_char(f)
            if token_code == 0:
                token_code = scanner(token_code, token_text)
            return token_code
        else:
            token_code = lookup_code(state, current_char(f))
            token_code = check_exceptions(token_code, token_text)
            if token_code == 0:
                token_code = scanner(token_code, token_text)
            return token_code

        
file_name = input("What is the file name: ")
f = open(file_name, 'r')

while current_char(f) != '':
    print(token_friendly_table[scanner()])
