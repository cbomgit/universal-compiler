﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MicroScanner
{
    class MicroParser
    {
        private MicroScanner scanner;

        public MicroParser(string filePath)
        {
            scanner = new MicroScanner(filePath);
        }
        
        private void SyntaxError(MicroScanner.Tokens token)
        {
            throw new Exception("Syntax Error: " + token.ToString());
        }

        private void Match(MicroScanner.Tokens token)
        {
            if (token != scanner.Scanner())
            {
                SyntaxError(token);
            }
        }

        public void SystemGoal()
        {
            Console.WriteLine("<system goal>");
            Program();
            Match(MicroScanner.Tokens.EofSym);
        }

        private void Program()
        {
            Console.WriteLine("<program> --> BEGIN <stmt list> END");
            Match(MicroScanner.Tokens.BeginSym);
            StmtList();
            Match(MicroScanner.Tokens.EndSym);
        }

        private void StmtList()
        {
            Console.WriteLine("<statement list> --> <statement | <statement> <statement list>");
            Statement();
            MicroScanner.Tokens next = scanner.NextToken();
            switch (next)
            {
                case MicroScanner.Tokens.Id:
                case MicroScanner.Tokens.ReadSym:
                case MicroScanner.Tokens.WriteSym:
                    StmtList();
                    break;
                default:
                    break;
            }
        }

        private void Statement()
        {
            MicroScanner.Tokens next = scanner.NextToken();
            switch (next)
            {
                case MicroScanner.Tokens.Id:
                    Console.WriteLine("<statement> --> <ident> := <expression>;");
                    Ident();
                    Match(MicroScanner.Tokens.AssinOp);
                    Expression();
                    Match(MicroScanner.Tokens.SemiColon);
                    break;
                case MicroScanner.Tokens.ReadSym:
                    Console.WriteLine("<statement> --> Read( <idlist> );");
                    Match(MicroScanner.Tokens.ReadSym);
                    Match(MicroScanner.Tokens.LParen);
                    IdList();
                    Match(MicroScanner.Tokens.RParen);
                    Match(MicroScanner.Tokens.SemiColon);
                    break;
                case MicroScanner.Tokens.WriteSym:
                    Console.WriteLine("<statement> --> Write( <exprlist> );");
                    Match(MicroScanner.Tokens.WriteSym);
                    Match(MicroScanner.Tokens.LParen);
                    ExprList();
                    Match(MicroScanner.Tokens.RParen);
                    Match(MicroScanner.Tokens.SemiColon);
                    break;
                default:
                    SyntaxError(next);
                    break;
            }
        }

        private void Ident()
        {
            Console.WriteLine("<ident> --> ID");
            Match(MicroScanner.Tokens.Id);
        }

        private void Primary()
        {
            MicroScanner.Tokens next = scanner.NextToken();
            switch (next)
            {
                case MicroScanner.Tokens.LParen:
                    Console.WriteLine("<primary> --> ( <expression> );");
                    Match(MicroScanner.Tokens.LParen);
                    Expression();
                    Match(MicroScanner.Tokens.RParen);
                    break;
                case MicroScanner.Tokens.Id:
                    Console.WriteLine("<primary> --> <ident>");
                    Ident();
                    break;
                case MicroScanner.Tokens.IntLiteral:
                    Console.WriteLine("<primary> --> IntLiteral");
                    Match(MicroScanner.Tokens.IntLiteral);
                    break;
                default:
                    SyntaxError(next);
                    break;
            }
        }

        private void Expression()
        {

            Primary();
            if (scanner.NextToken() == MicroScanner.Tokens.PlusOp || scanner.NextToken() == MicroScanner.Tokens.MinusOp)
            {
                Console.WriteLine("<expression> --> <primary> +/- <expression>");
                AddOp();
                Expression();
            }
            else
            {
                Console.WriteLine("<expression> --> <primary>");
            }
        }

        private void IdList()
        {
            Ident();
            if (scanner.NextToken() == MicroScanner.Tokens.Comma)
            {
                Console.WriteLine("<idlist> --> <ident>, <idlist>");
                Match(MicroScanner.Tokens.Comma);
                IdList();
            }
            else
            {
                Console.WriteLine("<idlist> --> <ident>");
            }
        }

        private void ExprList()
        {
            Expression();
            if (scanner.NextToken() == MicroScanner.Tokens.Comma)
            {
                Console.WriteLine("<exprlist> --> <expression>, <exprlist>");
                Match(MicroScanner.Tokens.Comma);
                ExprList();
            }
            else
            {
                Console.WriteLine("<exprlist> --> <expression>");
            }
        }

        private void AddOp()
        {
            if (scanner.NextToken() == MicroScanner.Tokens.PlusOp)
            {
                Match(MicroScanner.Tokens.PlusOp);
            }
            else if(scanner.NextToken() == MicroScanner.Tokens.MinusOp)
            {
                Match(MicroScanner.Tokens.MinusOp);
            }
            else
            {
                SyntaxError(scanner.NextToken());
            }
            
        }
    }
}
