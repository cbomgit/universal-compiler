﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Text.RegularExpressions;

namespace MicroScanner
{
    class MicroScanner
    {
        public enum Tokens
        {
            BeginSym,
            EndSym,
            ReadSym,
            WriteSym,
            Id,
            IntLiteral,
            LParen,
            RParen,
            SemiColon,
            Comma,
            AssinOp,
            PlusOp,
            MinusOp,
            EofSym,
            None
        }


        private string charBuffer;
        private StreamReader fileStream;


        public MicroScanner(string path)
        {
            fileStream = new StreamReader(path);
        }

        public Tokens NextToken()
        {
            return Tokens.None;
        }

        public Tokens Scanner()
        {

            Regex letters = new Regex(@"[a-zA-Z]");
            Regex alphaNumeric = new Regex(@"[a-zA-Z0-9_]");
            Regex digits = new Regex(@"[0-9]");

            ClearBuffer();
            
            if(fileStream.Peek() == -1)
            {
                return Tokens.EofSym;
            }
            else
            {
                while(fileStream.Peek() != -1)
                {
                    char currentChar = (char)(fileStream.Read());
                    //Code to recognize new lines and space characters
                    if (currentChar == ' ' || currentChar == '\t' || currentChar == '\n' || currentChar == '\r' || (currentChar == '\r' && ((char)(fileStream.Peek())) != '\n'))
                    {
                        //do nothing
                    }
                    else if (letters.Match(currentChar.ToString()).Success) // Code to recognize identifiers and reserved words
                    {
                        BufferChar(currentChar);
                        char nextChar = (char)(fileStream.Peek());
                        while (alphaNumeric.Match(nextChar.ToString()).Success)
                        {

                            BufferChar(nextChar);
                            fileStream.Read();
                            nextChar = (char)(fileStream.Peek());
                        }
                        return CheckReserved();
                    }
                    else if (digits.Match(currentChar.ToString()).Success) //code for int literals
                    {
                        BufferChar(currentChar);
                        char nextChar = (char)(fileStream.Peek());
                        while (digits.Match(nextChar.ToString()).Success)
                        {
                            BufferChar(nextChar);
                            fileStream.Read();
                            nextChar = (char)(fileStream.Peek());
                        }
                        return Tokens.IntLiteral;
                    }
                    else if (currentChar == '(')
                    {
                        return Tokens.LParen;
                    }
                    else if (currentChar == ')')
                    {
                        return Tokens.RParen;
                    }
                    else if (currentChar == ';')
                    {
                        return Tokens.SemiColon;
                    }
                    else if (currentChar == ',')
                    {
                        return Tokens.Comma;
                    }
                    else if(currentChar == '+')
                    {
                        return Tokens.PlusOp;
                    }
                    else if(currentChar == ':') //code for assignment operator
                    {
                        if(((char)(fileStream.Peek())) == '=')
                        {
                            fileStream.Read();
                            return Tokens.AssinOp;
                        }
                        else
                        {
                            LexicalError(charBuffer); 
                        }
                    }
                    else if(currentChar == '-') //code for comments (ignored) or minus operator
                    {
                        char nextChar = (char)(fileStream.Peek());
                        if(nextChar == '-')
                        {
                            currentChar = (char)(fileStream.Peek());
                            while(currentChar != '\n' && currentChar != '\r' && (currentChar != '\r' && ((char)(fileStream.Peek())) != '\n'))
                            {
                                fileStream.Read();
                                currentChar = (char)(fileStream.Peek());
                            }
                        }
                        else
                        {
                            return Tokens.MinusOp;
                        }
                    }
                    else
                    {
                        LexicalError("Not able to create a token. Current char buffer is " + charBuffer);
                    }
                }
            }
            fileStream.Close();
            return Tokens.EofSym;
            
        }

        private void ClearBuffer()
        {
            charBuffer = "";
        }

        private void BufferChar(char c)
        {
            charBuffer += c;
        }

        private Tokens CheckReserved()
        {
            if (charBuffer.Equals("BEGIN"))
            {
                return Tokens.BeginSym;
            }
            else if (charBuffer.Equals("END"))
            {
                return Tokens.EndSym;
            }
            else if (charBuffer.Equals("READ"))
            {
                return Tokens.ReadSym;
            }
            else if (charBuffer.Equals("WRITE"))
            {
                return Tokens.WriteSym;
            }
            else
            {
                return Tokens.Id;
            }
        }

        private void LexicalError(string s)
        {
            fileStream.Close();
            throw new Exception("Lexical error: " + s);
        }

        static void Main(string[] args)
        {

            string inputFilePath = @"SampleMicroProgram.txt";
            Console.Write("Enter the name of the file in the current directory to read in: ");
            inputFilePath = Console.ReadLine();

            try
            {
                    MicroScanner s = new MicroScanner(inputFilePath);
                    Tokens t =  s.Scanner();
                    while(t != Tokens.EofSym)
                    {
                        Console.WriteLine(t);
                        t = s.Scanner();
                    }

                    Console.WriteLine(t);
                
                Console.WriteLine("Press Enter to quit...");
                Console.ReadLine();
            }
            catch(Exception e)
            {
                Console.WriteLine("There was an error.");
                Console.WriteLine(e.Message);

                Console.WriteLine("Press Enter to quit...");
                Console.ReadLine();

            }
        }
    }
}
