#!/usr/bin/env python

import re
import pdb
#action constants
MOVE_APPEND = "MoveAppend"
MOVE_NO_APPEND = "MoveNoAppend"
HALT_APPEND = "HaltAppend"
HALT_NO_APPEND = "HaltNoAppend"
HALT_REUSE = "HaltReuse"
ERROR = "Error"

state_action_map = { 
            "$abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ": {
                0:{"state":1, "action":MOVE_APPEND },
                1:{"state":1, "action":MOVE_APPEND},
                2:{"state":12,"action":HALT_REUSE},
                3:{"state":13,"action":HALT_REUSE},
                4:{"state":21,"action":HALT_REUSE},
                5:{"state":5, "action":MOVE_NO_APPEND},
                6:{"state":-1,"action":ERROR}
            },
            "0123456789": {
                0:{"state":2, "action":MOVE_APPEND},
                1:{"state":1, "action":MOVE_APPEND},
                2:{"state":2, "action":MOVE_APPEND},
                3:{"state":13,"action":HALT_REUSE},
                4:{"state":21,"action":HALT_REUSE},
                5:{"state":5, "action":MOVE_NO_APPEND},
                6:{"state":-1,"action":ERROR}
            },
            " ": {
                0:{"state":3, "action":MOVE_NO_APPEND},
                1:{"state":11,"action":HALT_NO_APPEND},
                2:{"state":12,"action":HALT_NO_APPEND},
                3:{"state":3, "action":MOVE_NO_APPEND},
                4:{"state":21,"action":HALT_NO_APPEND},
                5:{"state":5, "action":MOVE_NO_APPEND},
                6:{"state":-1,"action":ERROR}
            },
            "+": {
                0:{"state":14,"action":HALT_NO_APPEND},
                1:{"state":11,"action":HALT_REUSE},
                2:{"state":12,"action":HALT_REUSE},
                3:{"state":13,"action":HALT_REUSE},
                4:{"state":21,"action":HALT_REUSE},
                5:{"state":5, "action":MOVE_NO_APPEND},
                6:{"state":-1,"action":ERROR}
            },
            "-": {
                0:{"state":4, "action":MOVE_APPEND},
                1:{"state":11,"action":HALT_REUSE},
                2:{"state":12,"action":HALT_REUSE},
                3:{"state":13,"action":HALT_REUSE},
                4:{"state":5, "action":MOVE_NO_APPEND},
                5:{"state":5, "action":MOVE_NO_APPEND},
                6:{"state":-1,"action":ERROR}
            },
            "=": {
                0:{"state":-1,"action":ERROR},
                1:{"state":11,"action":HALT_REUSE},
                2:{"state":12,"action":HALT_REUSE},
                3:{"state":13,"action":HALT_REUSE},
                4:{"state":21,"action":HALT_REUSE},
                5:{"state":5, "action":MOVE_NO_APPEND},
                6:{"state":16,"action":HALT_APPEND}
            },
            ":": {
                0:{"state":6,"action":MOVE_APPEND},
                1:{"state":11,"action":HALT_REUSE},
                2:{"state":12,"action":HALT_REUSE},
                3:{"state":13,"action":HALT_REUSE},
                4:{"state":21,"action":HALT_REUSE},
                5:{"state":5, "action":MOVE_NO_APPEND},
                6:{"state":-1,"action":ERROR}
            },
            ",": {
                0:{"state":17,"action":HALT_APPEND},
                1:{"state":11,"action":HALT_REUSE},
                2:{"state":12,"action":HALT_REUSE},
                3:{"state":13,"action":HALT_REUSE},
                4:{"state":21,"action":HALT_REUSE},
                5:{"state":5, "action":MOVE_NO_APPEND},
                6:{"state":-1,"action":ERROR}
            },
            ";": {
                0:{"state":18,"action":HALT_APPEND},
                1:{"state":11,"action":HALT_REUSE},
                2:{"state":12,"action":HALT_REUSE},
                3:{"state":13,"action":HALT_REUSE},
                4:{"state":21,"action":HALT_REUSE},
                5:{"state":5, "action":MOVE_NO_APPEND},
                6:{"state":-1,"action":ERROR}
            },
            "(": {
                0:{"state":19,"action":HALT_APPEND},
                1:{"state":11,"action":HALT_REUSE},
                2:{"state":12,"action":HALT_REUSE},
                3:{"state":13,"action":HALT_REUSE},
                4:{"state":21,"action":HALT_REUSE},
                5:{"state":5, "action":MOVE_NO_APPEND},
                6:{"state":-1,"action":ERROR}
            },
            ")": {
                0:{"state":20,"action":HALT_APPEND},
                1:{"state":11,"action":HALT_REUSE},
                2:{"state":12,"action":HALT_REUSE},
                3:{"state":13,"action":HALT_REUSE},
                4:{"state":21,"action":HALT_REUSE},
                5:{"state":5, "action":MOVE_NO_APPEND},
                6:{"state":-1,"action":ERROR}
            },
            "_": {
                0:{"state":-1,"action":ERROR},
                1:{"state":1, "action":MOVE_APPEND},
                2:{"state":12,"action":HALT_REUSE},
                3:{"state":13,"action":HALT_REUSE},
                4:{"state":-1,"action":ERROR},
                5:{"state":5, "action":MOVE_NO_APPEND},
                6:{"state":-1,"action":ERROR}
            },
            "\t": {
                0:{"state":3, "action":MOVE_NO_APPEND},
                1:{"state":11,"action":HALT_NO_APPEND},
                2:{"state":12,"action":HALT_NO_APPEND},
                3:{"state":3, "action":MOVE_NO_APPEND},
                4:{"state":21,"action":HALT_NO_APPEND},
                5:{"state":5, "action":MOVE_NO_APPEND},
                6:{"state":-1,"action":ERROR}
            },
            "\n": {
                0:{"state":3, "action":MOVE_NO_APPEND},
                1:{"state":11,"action":HALT_NO_APPEND},
                2:{"state":12,"action":HALT_NO_APPEND},
                3:{"state":3, "action":MOVE_NO_APPEND},
                4:{"state":21,"action":HALT_NO_APPEND},
                5:{"state":15,"action":HALT_NO_APPEND},
                6:{"state":-1,"action":ERROR}
            },
            "?": {
                0:{"state":-1,"action":ERROR},
                1:{"state":-1,"action":ERROR},
                2:{"state":-1,"action":ERROR},
                3:{"state":-1,"action":ERROR},
                4:{"state":-1,"action":ERROR},
                5:{"state":5, "action":MOVE_NO_APPEND},
                6:{"state":-1,"action":ERROR}
            },
}

#Constant values for tokens
#token code constants
INT_LITERAL = 1
EMPTY_SPACE = 0
PLUS_OP = 3
COMMENT = 4
ASSIGN_OP = 5
COMMA = 6
SEMICOLON = 7
LPAREN = 8
RPAREN = 9
MINUS_OP = 10
BEGIN = 11
END = 12
READ = 13
WRITE = 14
ID = 15
DOLLARSIGN = 16
LAMBDA = 17


#used in transition table to tranlsate final states to tokens

token_codes = { 11: ID,
                12: INT_LITERAL,
                13: EMPTY_SPACE,
                14: PLUS_OP,
                15: COMMENT,
                16: ASSIGN_OP,
                17: COMMA,
                18: SEMICOLON,
                19: LPAREN,
                20: RPAREN,
                21: MINUS_OP,
                22: BEGIN,
                23: END,
                24: READ,
                25: WRITE
}

#used for printing token codes
token_friendly_table = {
    INT_LITERAL:"IntLiteral",
    EMPTY_SPACE:"EmptySpace",
    PLUS_OP:"+",
    COMMENT:"Comment",
    ASSIGN_OP:":=",
    COMMA:",",
    SEMICOLON:";",
    LPAREN:"(",
    RPAREN:")",
    MINUS_OP:"-",
    BEGIN:"Begin",
    END:"End",
    READ:"Read",
    WRITE:"Write",
    ID:"Id",
    DOLLARSIGN:"$",
    LAMBDA:"Lambda"
}

exception_table = {
    "BEGIN":BEGIN,
    "END":END,
    "READ":READ,
    "WRITE":WRITE,
    "$":DOLLARSIGN,
    "LAMBDA":LAMBDA,
    "INTLITERAL":INT_LITERAL
}

def get_state_action_pair(curr_state, curr_char):
    
    pair = [value for key, value in state_action_map.items() if curr_char in key]
    if len(pair) == 0:
        return state_action_map['?'][curr_state]
    else:
        return pair[0][curr_state]
        
def lookup_code(curr_state, curr_char):
    
    state_action_pair = get_state_action_pair(curr_state, curr_char)
    next_state = state_action_pair['state']
    return token_codes[next_state]

def check_exceptions(token_code, token_text):
    if token_text.upper() in exception_table:
        return exception_table[token_text.upper()]
    else:
        return token_code
        
def lexical_error(line, ndx, state, token_code = 0, token_text=''):
    print("lexical error")
    exit()

def scanner(line, terms,ndx=0, token_code = 0, token_text = ''):
    state = 0
    #pdb.set_trace()
    while ndx < len(line):
        state_action_pair = get_state_action_pair(state, line[ndx])
        next_action = state_action_pair['action']
        if next_action == ERROR:
            print("lexical error")
            exit()
        elif next_action == MOVE_APPEND:
            state = state_action_pair['state']
            token_text = token_text + line[ndx]
            ndx += 1
        elif next_action == MOVE_NO_APPEND:
            state = state_action_pair['state']
            ndx += 1
        elif next_action == HALT_APPEND:
            token_code = lookup_code(state, line[ndx])
            token_text = token_text + line[ndx]
            token_code = check_exceptions(token_code, token_text)
            ndx += 1
            if token_code == 0:
                ret = scanner(line, terms, ndx, token_code, token_text)
                ndx = ret["line position"]
                token_code = ret["code"]
            terms.add(token_friendly_table[token_code])
            return {"code":token_code,"line position":ndx}
        elif next_action == HALT_NO_APPEND:
            token_code = lookup_code(state, line[ndx])
            token_code = check_exceptions(token_code, token_text)
            ndx += 1
            if token_code == 0:
                ret = scanner(line, terms, ndx, token_code, token_text)
                ndx = ret["line position"]
                token_code = ret["code"]
            terms.add(token_friendly_table[token_code])
            return {"code":token_code,"line position":ndx}
        else:
            token_code = lookup_code(state, line[ndx])
            token_code = check_exceptions(token_code, token_text)
            if token_code == 0:
                ret = scanner(line, terms, ndx, token_code, token_text)
                ndx = ret["line position"]
                token_code = ret["code"]
            terms.add(token_friendly_table[token_code])
            return {"code":token_code,"line position":ndx}
    return {"code":None, "line position":ndx}
                
f = open("productions.txt", "r")

rhs = set()
lhs = set()
non_terminals = set()
productions = set()
terminals = set()

for line in f:
    productions.add(line.strip("\n"))
    r = line.split(" -> ")[1]
    l= line.split(" -> ")[0]
    matches = re.findall(r"<[\w ]+>", r)
    for m in matches:
        non_terminals.add(m)
    non_terminals.add(l)
    lhs.add(l)
    rhs.add(r)

print("Left hand sides")
print(lhs)
print("Right hand sides")
print(rhs)
print("Non terminals")
print(non_terminals)
print("Productions")
print(productions)
f.close()
new_rhs = set()

for line in rhs:
    matches = re.findall(r"<[\w ]+>", line)
    for m in matches:
        line = line.replace(m, "nonterminal")
    new_rhs.add(line)

for line in new_rhs:
    ndx = 0
    while ndx < len(line):
        ret = scanner(line, terminals, ndx)
        ndx = ret["line position"]
        if ret["code"]:
            terminals.add(token_friendly_table[ret["code"]])

print("Terminals")
print(terminals)
